package logger

import (
	"github.com/sirupsen/logrus"
)

func GetLogger(moduleName string) *logrus.Entry {
	contextLogger := logrus.WithFields(logrus.Fields{
		"module": moduleName,
	})
	return contextLogger
}

func GetGormLogger(moduleName string) *GormLogger {
	contextLogger := logrus.WithFields(logrus.Fields{
		"module": moduleName,
	})
	return NewGormLogger(contextLogger)
}
