package logger

import (
	"github.com/sirupsen/logrus"
)

type LogOutputFormat string

const (
	TextOutputFormat LogOutputFormat = "text"
	JsonOutputFormat LogOutputFormat = "json"
)

type LogConfig struct {
	Format LogOutputFormat
	Level  string
}

func GetDefaults() LogConfig {
	defaultLc := LogConfig{
		Format: TextOutputFormat,
		Level:  logrus.InfoLevel.String(),
	}
	return defaultLc
}

func (lc *LogConfig) GetLevelNum() logrus.Level {
	levelNum, _ := logrus.ParseLevel(lc.Level) //TODO: check level and return Info?
	return levelNum
}
