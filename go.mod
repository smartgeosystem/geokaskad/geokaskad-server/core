module gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core

go 1.16

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.7.3
	github.com/hashicorp/go-version v1.2.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	gorm.io/driver/postgres v1.1.0
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.10
)
