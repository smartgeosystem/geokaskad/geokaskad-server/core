package router

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func NewDefaultRouter() *mux.Router {
	router := mux.NewRouter()
	router.StrictSlash(true)
	router.Use(handlers.CompressHandler)
	return router
}
