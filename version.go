package core

import "github.com/hashicorp/go-version"

const Version = "0.0.1"

func GetVersion() *version.Version {
	v, _ := version.NewVersion(Version)
	return v
}
