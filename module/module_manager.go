package module

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/config"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/logger"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/metadata"
)

func NewModuleManager() *ModuleManager {
	return &ModuleManager{
		registeredModules: map[string]GeokaskadModule{},
		disabledModules:   map[string]string{},
		logger:            logger.GetLogger("core.module"),
	}
}

type ModuleManager struct {
	registeredModules map[string]GeokaskadModule
	disabledModules   map[string]string
	logger            *logrus.Entry
}

func (mm *ModuleManager) RegisterModule(module GeokaskadModule) error {
	if module == nil {
		return fmt.Errorf("nil pointer to module")
	}
	if _, ok := mm.registeredModules[module.ID()]; ok {
		return fmt.Errorf("module with same type already registered")
	}
	mm.registeredModules[module.ID()] = module
	return nil
}

func (mm *ModuleManager) RegisterModules(modules []GeokaskadModule) error {
	for _, module := range modules {
		err := mm.RegisterModule(module)
		if err != nil {
			return err
		}
	}
	return nil
}

// DisableModuleByName - Work only before modules run!
func (mm *ModuleManager) DisableModuleById(id string, reason string) {
	mm.disabledModules[id] = reason
}

func (mm *ModuleManager) checkModulesVersionCompat() {
	// https://pkg.go.dev/github.com/hashicorp/go-version#Constraints
	coreV := core.GetVersion()
	mm.logger.Debugf("Check modules version compat. Core version: %s", coreV)
	for key, module := range mm.registeredModules {
		minCoreV, maxCoreV := module.CompatCoreVersion()
		if minCoreV.LessThanOrEqual(coreV) && maxCoreV.GreaterThanOrEqual(coreV) {
			mm.logger.Debugf("Module '%s'. Core version is compat", module.ID())
		} else {
			mm.logger.Warningf("Module '%s'. Core version is not compat. Need: [%s, %s]. The module will be disabled!",
				module.ID(), minCoreV.String(), maxCoreV.String())
			mm.DisableModuleById(key, "unsupported core version")
		}
	}
}

func (mm *ModuleManager) checkModulesMetadataStorageCompat(storageType metadata.MetadataStorageType) {
	mm.logger.Debugf("Check modules MetadataStorage compat. Current type: %s", storageType)
	for key, module := range mm.registeredModules {
		storageTypes := module.CompatMetadataStoreTypes()
		storageTypeCompat := false
		for _, v := range storageTypes {
			if v == storageType {
				storageTypeCompat = true
				break
			}
		}
		if storageTypeCompat {
			mm.logger.Debugf("Module '%s'. Metadata storage type is compat", module.ID())
		} else {
			mm.logger.Warningf("Module '%s'. Metadata storage type is not compat. Need: %s. The module will be disabled!",
				module.ID(), storageTypes)
			mm.DisableModuleById(key, "unsupported metadata storage type")
		}
	}
}

func (mm *ModuleManager) configurateModules(configManager *config.ConfigManager) {
	mm.logger.Debugf("Configurate modules")
	for key, module := range mm.GetActiveModules() {
		if err := module.ConfigurateModule(configManager); err == nil {
			mm.logger.Debugf("Module '%s'. The module successfully configured", module.ID())
		} else {
			mm.logger.Warningf("Module '%s'. Error on configuration: %s. The module will be disabled!",
				module.ID(), err)
			mm.DisableModuleById(key, "error on module configure")
		}
	}
}

func (mm *ModuleManager) MigrateModulesMetadata(storage metadata.MetadataStorage) {
	mm.logger.Debugf("Modules metadata migration")
	for key, module := range mm.GetActiveModules() {
		if err := module.MigrateMetadata(storage); err == nil {
			mm.logger.Debugf("Module '%s'. Metadata successfully migrated", module.ID())
		} else {
			mm.logger.Warningf("Module '%s'. Error on metadata migrate: %s. The module will be disabled!",
				module.ID(), err)
			mm.DisableModuleById(key, "error on module metadata migration")
		}
	}
}

func (mm *ModuleManager) initModulesRoutes(router *mux.Router) {
	mm.logger.Debugf("Modules routes initialization")
	for key, module := range mm.GetActiveModules() {
		if err := module.InitRouter(router); err == nil {
			mm.logger.Debugf("Module '%s'. Routes successfully initialized", module.ID())
		} else {
			mm.logger.Warningf("Module '%s'. Error on routes initialization: %s. The module will be disabled!",
				module.ID(), err)
			mm.DisableModuleById(key, "error on module router init")
		}
	}
}

func (mm *ModuleManager) runModules() {
	mm.logger.Debugf("Modules run")
	for key, module := range mm.GetActiveModules() {
		if err := module.RunModule(); err == nil {
			mm.logger.Debugf("Module '%s'. Successfully started", module.ID())
		} else {
			mm.logger.Warningf("Module '%s'. Error on module run: %s. The module will be disabled!",
				module.ID(), err)
			mm.DisableModuleById(key, "error on module run")
		}
	}
}

func (mm *ModuleManager) StartModules(configManager *config.ConfigManager, storage metadata.MetadataStorage, router *mux.Router) {
	mm.checkModulesVersionCompat()
	mm.checkModulesMetadataStorageCompat(storage.GetType())
	mm.configurateModules(configManager)
	mm.MigrateModulesMetadata(storage)
	mm.initModulesRoutes(router)
	mm.runModules()
}

func (mm *ModuleManager) StopModules() {
	for _, module := range mm.GetActiveModules() {
		if err := module.StopModule(); err != nil {
			mm.logger.Warningf("Module '%s'. Error on module stop: %s.", module.ID(), err)
		}
	}
}

func (mm *ModuleManager) GetRegisteredModules() map[string]GeokaskadModule {
	registeredMap := make(map[string]GeokaskadModule, len(mm.registeredModules))
	for key, value := range mm.registeredModules {
		registeredMap[key] = value
	}
	return registeredMap
}

func (mm *ModuleManager) GetActiveModules() map[string]GeokaskadModule {
	activeMap := make(map[string]GeokaskadModule, len(mm.registeredModules))
	for key, value := range mm.registeredModules {
		_, disabled := mm.disabledModules[key]
		if !disabled {
			activeMap[key] = value
		}
	}
	return activeMap
}
