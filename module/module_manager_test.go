package module

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/hashicorp/go-version"
	"github.com/stretchr/testify/assert"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/config"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/metadata"
	"testing"
)

// MOCKS
type GeokaskadModuleMock struct {
	id                   string
	description          string
	version              version.Version
	minCoreVersion       version.Version
	maxCoreVersion       version.Version
	metadataStorageTypes []metadata.MetadataStorageType
	errorOnConfigurate   bool
	errorOnMigrate       bool
	errorOnInitRouter    bool
	errorOnRunModule     bool
	errorOnStop          bool
}

func (g GeokaskadModuleMock) ID() string {
	return g.id
}

func (g GeokaskadModuleMock) Description() string {
	return g.description
}

func (g GeokaskadModuleMock) Version() version.Version {
	return g.version
}

func (g GeokaskadModuleMock) CompatCoreVersion() (version.Version, version.Version) {
	return g.minCoreVersion, g.maxCoreVersion
}

func (g GeokaskadModuleMock) CompatMetadataStoreTypes() []metadata.MetadataStorageType {
	return g.metadataStorageTypes
}

func (g GeokaskadModuleMock) RequiredModules() []string {
	panic("implement me")
}

func (g GeokaskadModuleMock) GetDefaultConfigValues() (string, interface{}) {
	panic("implement me")
}

func (g GeokaskadModuleMock) ConfigurateModule(configManager *config.ConfigManager) error {
	if g.errorOnConfigurate {
		return fmt.Errorf("test error on configurate")
	}
	return nil
}

func (g GeokaskadModuleMock) MigrateMetadata(storage metadata.MetadataStorage) error {
	if g.errorOnMigrate {
		return fmt.Errorf("test error on migrate")
	}
	return nil
}

func (g GeokaskadModuleMock) InitRouter(router *mux.Router) error {
	if g.errorOnInitRouter {
		return fmt.Errorf("test error on init router")
	}
	return nil
}

func (g GeokaskadModuleMock) RunModule() error {
	if g.errorOnRunModule {
		return fmt.Errorf("test error on run module")
	}
	return nil
}

func (g GeokaskadModuleMock) StopModule() error {
	if g.errorOnStop {
		return fmt.Errorf("test error on stop module")
	}
	return nil

}

type MetadataStorageMock struct{}

const MetadataStorageTypeTest metadata.MetadataStorageType = "test"

func (m MetadataStorageMock) GetType() metadata.MetadataStorageType {
	return MetadataStorageTypeTest
}

func (m MetadataStorageMock) GetSubType() string {
	return "subtest"
}

func (m MetadataStorageMock) GetConnection() interface{} {
	return nil
}

// TESTS CONSTS
const (
	testModuleId1 = "test_module_1"
	testModuleId2 = "test_module_2"
)

// TESTS

func TestModuleManager_GetRegisteredModules(t *testing.T) {
	mm := NewModuleManager()
	modules := mm.GetRegisteredModules()
	assert.NotNil(t, modules)
	assert.Equal(t, 0, len(modules))
}

func TestModuleManager_GetActiveModules(t *testing.T) {
	mm := NewModuleManager()
	modules := mm.GetActiveModules()
	assert.NotNil(t, modules)
	assert.Equal(t, 0, len(modules))
}

func TestModuleManager_GetActiveModules_WithoutDisabled(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1}
	testModule2 := GeokaskadModuleMock{id: testModuleId2}
	mm := NewModuleManager()

	err := mm.RegisterModules([]GeokaskadModule{testModule1, testModule2})
	assert.NoError(t, err, "Unexpected error on register")

	modules := mm.GetActiveModules()
	assert.NotNil(t, modules)
	assert.Equal(t, 2, len(modules))

	gotModule, ok := modules[testModuleId1]
	assert.True(t, ok, "Module not found as registered")
	assert.Equal(t, testModule1, gotModule)

	gotModule, ok = modules[testModuleId2]
	assert.True(t, ok, "Module not found as registered")
	assert.Equal(t, testModule2, gotModule)
}

func TestModuleManager_GetActiveModules_WithDisabled(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1}
	testModule2 := GeokaskadModuleMock{id: testModuleId2}
	mm := NewModuleManager()

	err := mm.RegisterModules([]GeokaskadModule{testModule1, testModule2})
	assert.NoError(t, err, "Unexpected error on register")
	mm.DisableModuleById(testModuleId1, "tests")

	modules := mm.GetActiveModules()
	assert.NotNil(t, modules)
	assert.Equal(t, 1, len(modules))

	gotModule, ok := modules[testModuleId2]
	assert.True(t, ok, "Module not found as active")
	assert.Equal(t, testModule2, gotModule)

	gotModule, ok = modules[testModuleId1]
	assert.False(t, ok, "Module found as active")

	// Check registered not changed
	registered := mm.GetRegisteredModules()
	assert.NotNil(t, registered)
	assert.Equal(t, 2, len(registered))
}

func TestModuleManager_RegisterModule(t *testing.T) {
	testModule := GeokaskadModuleMock{id: testModuleId1}
	mm := NewModuleManager()

	err := mm.RegisterModule(testModule)
	assert.NoError(t, err, "Error on register")

	goModule, ok := mm.GetRegisteredModules()[testModuleId1]
	assert.True(t, ok, "Module not found as registered")
	assert.Equal(t, testModule, goModule)
}

func TestModuleManager_RegisterModule_Nil(t *testing.T) {
	mm := NewModuleManager()

	err := mm.RegisterModule(nil)
	assert.Error(t, err, "No expected error")
}

func TestModuleManager_RegisterModule_Double(t *testing.T) {
	testModule := GeokaskadModuleMock{id: testModuleId1}
	mm := NewModuleManager()

	err := mm.RegisterModule(testModule)
	assert.NoError(t, err, "Unexpected error on register")

	err = mm.RegisterModule(testModule)
	assert.Error(t, err, "No expected error")
}

func TestModuleManager_RegisterModules(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1}
	testModule2 := GeokaskadModuleMock{id: testModuleId2}
	mm := NewModuleManager()

	err := mm.RegisterModules([]GeokaskadModule{testModule1, testModule2})
	assert.NoError(t, err, "Unexpected error on register")

	gotModule, ok := mm.GetRegisteredModules()[testModuleId1]
	assert.True(t, ok, "Module not found as registered")
	assert.Equal(t, testModule1, gotModule)

	gotModule, ok = mm.GetRegisteredModules()[testModuleId2]
	assert.True(t, ok, "Module not found as registered")
	assert.Equal(t, testModule2, gotModule)
}

func TestModuleManager_RegisterModules_Double(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1}
	mm := NewModuleManager()

	err := mm.RegisterModules([]GeokaskadModule{testModule1, testModule1})
	assert.Error(t, err, "No expected error on register")
}

func TestModuleManager_RegisterModules_Nil(t *testing.T) {
	mm := NewModuleManager()

	err := mm.RegisterModules([]GeokaskadModule{nil, nil})
	assert.Error(t, err, "No expected error on register")
}

func TestModuleManager_checkModulesVersionCompat_Compat(t *testing.T) {
	coreVer := core.GetVersion()

	testModule1 := GeokaskadModuleMock{id: testModuleId1, minCoreVersion: *coreVer, maxCoreVersion: *coreVer}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.checkModulesVersionCompat()
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.True(t, ok, "Module not found as active")
}

func TestModuleManager_checkModulesVersionCompat_Uncompat(t *testing.T) {
	uncompatVer, _ := version.NewVersion(core.Version + ".10")
	testModule1 := GeokaskadModuleMock{id: testModuleId1, minCoreVersion: *uncompatVer, maxCoreVersion: *uncompatVer}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.checkModulesVersionCompat()
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.False(t, ok, "Module is active. Expected as disabled")
}

func TestModuleManager_checkModulesMetadataStorageCompat_Compat(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, metadataStorageTypes: []metadata.MetadataStorageType{metadata.MetadataStorageTypeGorm}}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.checkModulesMetadataStorageCompat(metadata.MetadataStorageTypeGorm)
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.True(t, ok, "Module not found as active")
}

func TestModuleManager_checkModulesMetadataStorageCompat_Uncompat(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, metadataStorageTypes: []metadata.MetadataStorageType{}}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.checkModulesMetadataStorageCompat(metadata.MetadataStorageTypeGorm)
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.False(t, ok, "Module is active. Expected as disabled")
}

func TestModuleManager_configurateModules_Success(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnConfigurate: false}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.configurateModules(nil)
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.True(t, ok, "Module not found as active")
}

func TestModuleManager_configurateModules_Failed(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnConfigurate: true}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.configurateModules(nil)
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.False(t, ok, "Module is active. Expected as disabled")
}

func TestModuleManager_MigrateModulesMetadata_Success(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnMigrate: false}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.MigrateModulesMetadata(nil)
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.True(t, ok, "Module not found as active")
}

func TestModuleManager_MigrateModulesMetadata_Failed(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnMigrate: true}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.MigrateModulesMetadata(nil)
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.False(t, ok, "Module is active. Expected as disabled")
}

func TestModuleManager_initModulesRoutes_Success(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnInitRouter: false}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.initModulesRoutes(mux.NewRouter())
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.True(t, ok, "Module not found as active")
}

func TestModuleManager_initModulesRoutes_Failed(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnInitRouter: true}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.initModulesRoutes(mux.NewRouter())
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.False(t, ok, "Module is active. Expected as disabled")
}

func TestModuleManager_runModules_Success(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnRunModule: false}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.runModules()
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.True(t, ok, "Module not found as active")
}

func TestModuleManager_runModules_Failed(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnRunModule: true}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.runModules()
	_, ok := mm.GetActiveModules()[testModuleId1]
	assert.False(t, ok, "Module is active. Expected as disabled")
}

func TestModuleManager_StartModules_Success(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnStop: false}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.StartModules(nil, MetadataStorageMock{}, mux.NewRouter())
}

func TestModuleManager_StopModules_Success(t *testing.T) {
	testModule1 := GeokaskadModuleMock{id: testModuleId1, errorOnStop: false}
	mm := NewModuleManager()

	mm.RegisterModule(testModule1)
	mm.StopModules()
}

func TestNewModuleManager(t *testing.T) {
	if got := NewModuleManager(); got.registeredModules == nil || got.disabledModules == nil {
		t.Errorf("NewModuleManager()")
	}
}
