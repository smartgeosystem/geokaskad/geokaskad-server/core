package module

import (
	"github.com/gorilla/mux"
	"github.com/hashicorp/go-version"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/config"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/metadata"
)

type GeokaskadModule interface {
	// Meta
	ID() string
	Description() string
	Version() version.Version

	// Compatibility
	CompatCoreVersion() (version.Version, version.Version)
	CompatMetadataStoreTypes() []metadata.MetadataStorageType
	RequiredModules() []string

	// Configuration
	GetDefaultConfigValues() (string, interface{})

	// Life cycle
	ConfigurateModule(configManager *config.ConfigManager) error
	MigrateMetadata(storage metadata.MetadataStorage) error
	InitRouter(router *mux.Router) error
	RunModule() error
	StopModule() error
}
