package http_server

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
)

var log = logrus.WithField("module", "http_server")

type Server struct {
	port       int
	router     *mux.Router
	httpServer http.Server
}

func (s *Server) Run() {
	s.httpServer.Handler = s.router
	s.httpServer.Addr = fmt.Sprintf(":%d", s.port)
	err := s.httpServer.ListenAndServe()
	if err != nil {
		log.Fatalln(err)
	}
}

func NewServer(port int, router *mux.Router) *Server {
	// Init server
	server := Server{
		port:       port,
		router:     router,
		httpServer: http.Server{},
	}
	return &server
}
