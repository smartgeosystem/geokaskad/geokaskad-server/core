package config

import (
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/logger"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/metadata"
)

type CoreConfig struct {
	Logger   logger.LogConfig
	Metadata metadata.MetadataStorageConfig
}

const CoreDefaultConfKey = "core"
const LoggerDefaultConfKey = "core.logger"
const MetadataDefaultConfKey = "core.metadata"

func GetDefaultConfigValues() (string, interface{}) {
	coreConfigDefault := CoreConfig{
		Logger:   logger.GetDefaults(),
		Metadata: metadata.GetDefaults(),
	}
	return CoreDefaultConfKey, coreConfigDefault
}
