package config

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
	"strings"
)

type ConfigManager struct {
	viper.Viper
}

func NewConfigManager() *ConfigManager {
	cm := ConfigManager{
		Viper: *viper.New(),
	}
	cm.SetConfigType("yaml")
	cm.SetEnvKeyReplacer(strings.NewReplacer("-", "_", ".", "_"))
	cm.AllowEmptyEnv(true)
	cm.AutomaticEnv()

	return &cm
}

func (cm *ConfigManager) LoadLocalConfig(configPath string) error {
	if configPath != "" {
		// Use config file from the parameters
		cm.SetConfigFile(configPath)
	} else {
		// Search config in current dir "config.yaml"
		cm.AddConfigPath(".")
		cm.SetConfigName("config")
	}

	// If a config file is found, read it in.
	if err := cm.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Errorf("Error on load local config file: %s not found!", cm.ConfigFileUsed())
		} else {
			log.Errorln("Error on load local config file:", err)
		}
		return err
	} else {
		log.Infoln("Using local config file:", cm.ConfigFileUsed())
	}
	return nil
}

func (cm *ConfigManager) LoadRemoteConfig(remoteType string, remoteAddr string, remoteKey string) error {
	// Try to load remote
	if err := cm.AddRemoteProvider(remoteType, remoteAddr, remoteKey); err != nil {
		log.Errorln("Error on append remote configuration", err)
		return err
	}
	if err := cm.ReadRemoteConfig(); err != nil {
		log.Errorln("Error on read remote config: ", err)
		return err
	} else {
		log.Infoln("Using remote config:", remoteType, remoteAddr, remoteKey)
	}

	return nil

	// CONSUL WATCHING NOT WORKED!!!
	// https://github.com/spf13/viper/issues/326

	// open a goroutine to watch remote changes forever
	//go func(){
	//	for {
	//		time.Sleep(time.Second * 5) // delay after each request
	//
	//		err := viper.WatchRemoteConfig()
	//		if err != nil {
	//			logger.Errorf("unable to read remote config: %v", err)
	//			continue
	//		}
	//		logger.Println("NewConfig!")
	//		logger.Println("App name: ", viper.GetString("core.name"))
	//	}
	//}()
}

func (cm *ConfigManager) SetMappedDefault(key string, value interface{}) {
	var resultMap map[string]interface{}
	jsonValue, _ := json.Marshal(value)
	json.Unmarshal(jsonValue, &resultMap)
	cm.SetDefault(key, resultMap)
}

func (cm *ConfigManager) Fix() {
	// Fix Bug with struct unmarshal
	// https://github.com/spf13/viper/issues/1012
	for _, key := range cm.AllKeys() {
		cm.Set(key, cm.Get(key))
	}
}
