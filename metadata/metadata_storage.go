package metadata

type MetadataStorageType string

type MetadataStorage interface {
	GetType() MetadataStorageType
	GetSubType() string

	GetConnection() interface{}
}
