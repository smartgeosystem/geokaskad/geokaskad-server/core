package metadata

import "gorm.io/driver/sqlite"

type MetadataStorageConfig struct {
	Type     MetadataStorageType
	Settings interface{}
}

func GetDefaults() MetadataStorageConfig {
	// Default: GORM with local sqlite
	defaultMC := MetadataStorageConfig{
		Type: MetadataStorageTypeGorm,
		Settings: GormMetadataStorageSettings{
			Driver: (sqlite.Dialector{}).Name(),
			DSN:    "geokaskad.sqlite",
		},
	}
	return defaultMC
}
