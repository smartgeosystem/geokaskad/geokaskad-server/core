package metadata

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
)

func GetMetadataStorage(config MetadataStorageConfig) (MetadataStorage, error) {
	if config.Type != MetadataStorageTypeGorm {
		return nil, fmt.Errorf("unsupported metadata storage type. only '%s' acceptable", MetadataStorageTypeGorm)
	}
	gormSettings := GormMetadataStorageSettings{}

	err := mapstructure.Decode(config.Settings, &gormSettings) // TODO: hack
	if err != nil {
		return nil, fmt.Errorf("error on decode metadata storage settings: %s", err)
	}

	ms, err := NewGormMetadataStorage(gormSettings)
	return ms, err
}
