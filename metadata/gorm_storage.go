package metadata

import (
	"fmt"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/logger"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"time"
)

const MetadataStorageTypeGorm MetadataStorageType = "gorm"

type GormMetadataStorageSettings struct {
	Driver string
	DSN    string
}

type GormMetadataStorage struct {
	db *gorm.DB
}

func NewGormMetadataStorage(settings GormMetadataStorageSettings) (*GormMetadataStorage, error) {
	if settings.Driver != (postgres.Dialector{}).Name() && settings.Driver != (sqlite.Dialector{}).Name() {
		return nil, fmt.Errorf("unsupported gorm driver. only '%s' and '%s' acceptable",
			(postgres.Dialector{}).Name(), (sqlite.Dialector{}).Name())
	}
	var dialector gorm.Dialector
	if settings.Driver == (postgres.Dialector{}).Name() {
		dialector = postgres.Open(settings.DSN)
	}
	if settings.Driver == (sqlite.Dialector{}).Name() {
		dialector = sqlite.Open(settings.DSN)
	}

	db, err := gorm.Open(dialector, &gorm.Config{
		Logger: logger.GetGormLogger("core.metadata"),
	})
	if err != nil {
		return nil, fmt.Errorf("error on init gorm metadata storage: %s", err)
	}

	// Setup conn pool
	sqlDb, err := db.DB()
	if err != nil {
		return nil, fmt.Errorf("error on init gorm metadata storage: %s", err)
	}

	sqlDb.SetMaxIdleConns(5) // TODO: Move to config?
	sqlDb.SetMaxOpenConns(100)
	sqlDb.SetConnMaxLifetime(time.Hour)

	return &GormMetadataStorage{
		db: db,
	}, nil
}

func (gms *GormMetadataStorage) GetType() MetadataStorageType {
	return MetadataStorageTypeGorm
}

func (gms *GormMetadataStorage) GetSubType() string {
	if gms.db != nil {
		return gms.db.Dialector.Name()
	} else {
		return ""
	}
}

func (gms *GormMetadataStorage) GetConnection() interface{} {
	return gms.db
}
